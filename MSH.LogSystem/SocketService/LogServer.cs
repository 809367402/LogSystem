﻿using Common;
using DTO;
using SuperSocket.SocketBase;
using SuperSocket.SocketBase.Protocol;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebSocket.Core;

namespace SocketService.Core
{
    public class LogServer : AppServer<LogSession>
    {
        public LogServer()
            : base(new DefaultReceiveFilterFactory<LogReceiveFilter, StringRequestInfo>())
        {
        }

        protected override void OnStarted()
        {
            Console.WriteLine("服务已启动");
            base.OnStarted();
        }

        protected override void OnStopped()
        {
            Console.WriteLine("服务已停止");
            base.OnStopped();
        }

        protected override bool RegisterSession(string sessionID, LogSession appSession)
        {
            Console.WriteLine("注册Session");
            return base.RegisterSession(sessionID, appSession);
        }

        protected override void OnNewSessionConnected(LogSession session)
        {
            Console.WriteLine("新客户端连接！");
            base.OnNewSessionConnected(session);
        }

        protected override void OnSessionClosed(LogSession session, CloseReason reason)
        {
            Console.WriteLine($"客户端关闭，关闭原因：{reason}");
            base.OnSessionClosed(session, reason);
        }

        protected override void OnSystemMessageReceived(string messageType, object messageData)
        {
            base.OnSystemMessageReceived(messageType, messageData);
        }

        protected override void ExecuteCommand(LogSession session, StringRequestInfo requestInfo)
        {
            base.ExecuteCommand(session, requestInfo);
        }

        protected override void UpdateServerStatus(StatusInfoCollection serverStatus)
        {
            foreach (var item in serverStatus.Values)
            {
                Logger.Info(item);
            }
            base.UpdateServerStatus(serverStatus);
        }

        protected override void OnServerStatusCollected(StatusInfoCollection bootstrapStatus, StatusInfoCollection serverStatus)
        {
            foreach (var item in bootstrapStatus.Values)
            {
                Logger.Info(item);
            }
            var info = GetTcpServiceInfo(serverStatus);
            //发出通知
            WebSocketManage.SendToAll(info);
            base.OnServerStatusCollected(bootstrapStatus, serverStatus);
        }

        protected TcpServiceInfo GetTcpServiceInfo(StatusInfoCollection serverStatus)
        {
            return new TcpServiceInfo()
            {
                CreatTime = serverStatus.CollectedTime,
                AvialableSendingQueueItems = serverStatus.GetValue("AvialableSendingQueueItems", 0),
                IsRunning = serverStatus.GetValue("IsRunning", false),
                MaxConnectionNumber = serverStatus.GetValue("MaxConnectionNumber", 0),
                RequestHandlingSpeed = $"{serverStatus["RequestHandlingSpeed"]}",
                ServiceName = serverStatus.Name,
                StartedTime = serverStatus.GetValue("StartedTime", DateTime.Now),
                TotalConnections = serverStatus.GetValue("TotalConnections", 0),
                TotalHandledRequests = $"{serverStatus["TotalHandledRequests"]}",
                TotalSendingQueueItems = $"{serverStatus["TotalSendingQueueItems"]}",
            };
        }
    }
}
