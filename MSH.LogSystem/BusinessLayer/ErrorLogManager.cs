﻿using BusinessLayer.Interface;
using DTO;
using MongoDbAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class ErrorLogManager : IErrorLogManager
    {
        private ErrorLogAccess _ErrorLogAccess = new ErrorLogAccess();

        public void DeleteLog(string id)
        {
            _ErrorLogAccess.DeleteLog(id);
        }

        public void DeleteLog(List<string> ids)
        {
            _ErrorLogAccess.DeleteLog(ids);
        }

        public List<LogInfo> QueryLogInfo(LogQuery logQuery)
        {
            var dao = new ErrorLogAccess();
            return dao.QueryLogRequest(logQuery);
        }
    }
}
