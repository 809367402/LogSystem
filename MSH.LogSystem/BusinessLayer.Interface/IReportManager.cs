﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Interface
{
    /// <summary>
    /// 报表管理
    /// </summary>
    public interface IReportManager : IDependency
    {
        /// <summary>
        /// 获取今日新增日志报表
        /// </summary>
        /// <returns></returns>
        DTO.LogAddCountReport GetLogAddCountReport();

        /// <summary>
        /// 获取日志总量报表
        /// </summary>
        /// <returns></returns>
        DTO.TotalLogReport GetTotalLogReport();
    }
}
