import axios from '@/libs/api.request';

const controller = 'api/Report/';

/**
 * 获取今日新增日志报表
 * @constructor
 */
export const QueryTodayCountReport = () => {
    return axios.request({
        url: controller + 'QueryTodayCountReport',
        method: 'get'
    });
};

/**
 * 获取日志总量报表
 * @constructor
 */
export const QueryTotalCountReport = () => {
    return axios.request({
        url: controller + 'QueryTotalCountReport',
        method: 'get'
    });
};
